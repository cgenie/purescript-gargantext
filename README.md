# Gargantext Purescript

## About this project

Gargantext is a collaborative web platform for the exploration of sets
of unstructured documents. It combines tools from natural language
processing, text-mining, complex networks analysis and interactive data
visualization to pave the way toward new kinds of interactions with your
digital corpora.

This software is a free software, developed by the CNRS Complex Systems
Institute of Paris Île-de-France (ISC-PIF) and its partners.

## Installation of this library

### Dependencies warning
This library purescript-gargantext is the Front End part of Gargantext.
you need the backend (haskell-gargantext) installation too.

### Installation steps

1. Add nodes_modules/.bin to your path
2. Execute ./build

## Note to the contributors

Please follow CONTRIBUTING.md
