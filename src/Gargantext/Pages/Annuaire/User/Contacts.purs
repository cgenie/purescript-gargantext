module Gargantext.Pages.Annuaire.User.Contacts
       (module Gargantext.Pages.Annuaire.User.Contacts.Types,
        module Gargantext.Pages.Annuaire.User.Contacts.Specs)
       where

import Gargantext.Pages.Annuaire.User.Contacts.Types
import Gargantext.Pages.Annuaire.User.Contacts.Specs
